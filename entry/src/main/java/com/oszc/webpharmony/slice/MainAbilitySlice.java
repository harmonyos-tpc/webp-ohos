package com.oszc.webpharmony.slice;

import com.oszc.webpharmony.ResourceTable;
import me.everything.webp.WebPDecoder;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.media.image.PixelMap;

import java.io.IOException;

public class MainAbilitySlice extends AbilitySlice {
    private Image mImage;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mImage = (Image)findComponentById(ResourceTable.Id_iv);

        try {
            Resource resource = getResourceManager().getResource(ResourceTable.Media_girl);
            byte[] buffer = new byte[resource.available()];
            resource.read(buffer);
            PixelMap pixelMap = WebPDecoder.getInstance().decodeWebP(buffer);
            mImage.setPixelMap(pixelMap);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }

//        getResourceManager().getResource(ResourceTable.)
 //       WebPDecoder.getInstance().decodeWebP()
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
