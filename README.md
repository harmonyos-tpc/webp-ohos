# webp
## webp for all! (openharmony devices)

webp is a library we use at EverythingMe Launcher since we love webp. We use it to save bandwidth as well as shrinking our APK sizes.

webp is an adaptation of chromium's webp decoder, and an addition of a JNI wrapper to easily use it it in your java code.


## How to use it?

```java
import me.everything.webp.WebPDecoder

PixelMap bitmap = WebPDecoder.getInstance().decodeWebP(bytes);
```
